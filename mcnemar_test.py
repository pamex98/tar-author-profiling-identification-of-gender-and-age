import numpy as np
import sys
from sklearn.metrics import confusion_matrix
from statsmodels.stats.contingency_tables import mcnemar

alpha = 0.05

# arguments should be : <path_to_first_model_predictions_on_test_set.npy> <path_to_second_model_predictions_on_test_set.npy>

if __name__ == '__main__':
    if len(sys.argv) != 3:
        raise RuntimeError("Invalid argument")

    first_model_preds = np.load(sys.argv[1])
    second_model_preds = np.load(sys.argv[2])

    cMatrix = confusion_matrix(first_model_preds, second_model_preds)
    print(cMatrix)

    # calculate McNemar test
    result = mcnemar(cMatrix, exact=False, correction=False)

    print(f'p = {result.pvalue}')

    if result.pvalue > alpha:
        print(f'Same proportions of errors (fail to reject H0) - p > alpha ({result.pvalue} > {alpha})')
    else:
        print(f'Different proportions of errors (reject H0) - p <= alpha ({result.pvalue} <= {alpha})')
