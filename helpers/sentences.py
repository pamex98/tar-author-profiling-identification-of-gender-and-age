from nltk import sent_tokenize

import numpy as np


### Sentences
def gather_sentences(text: str) -> list:
    if not isinstance(text, str):
        return []

    return sent_tokenize(text)


def count_sentences(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(gather_sentences(text))
