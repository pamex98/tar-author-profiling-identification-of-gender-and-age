def get_age_category(age: int) -> int:
    if age >= 13 and age <= 17:
        return 0
    elif age >=23 and age <= 27:
        return 1
    elif age >= 33 and age <= 48:
        return 2

# minimum occurence treshold
OC_TRESH = 5

def is_in_one_age_cat(arr):
    """Word has to be used more than once in only one age category"""
    if arr[0] < 1 and arr[1] < 1 and arr[2] > OC_TRESH:
        return True
    elif arr[0] < 1 and arr[1] > OC_TRESH and arr[2] < 1:
        return True
    elif arr[0] > OC_TRESH and arr[1] < 1 and arr[2] < 1:
        return True
    
    return False

def is_in_one_gender(arr):
    """Word has to be used more than once in only one gender category"""
    if arr[3] > OC_TRESH and arr[4] < 1:
        return True
    if arr[3] < 1 and arr[4] > OC_TRESH:
        return True
    return False
