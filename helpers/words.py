import re

import numpy as np


### Count words
def gather_words(text: str) -> list:
    if not isinstance(text, str):
        return []

    return re.findall(r"\b[^\d\W]+\b", text)


def count_words(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan
    return len(gather_words(text))
