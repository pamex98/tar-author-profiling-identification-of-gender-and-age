import re

import emoji

import numpy as np


### Emojis


def gather_emojis(text: str) -> list:
    if (not isinstance(text, str)) or (len(text.strip()) == 0):
        return []

    emoji_expaned_text = emoji.demojize(text)
    return re.findall(r"\:(.*?)\:", emoji_expaned_text)


def count_emojis(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    list_of_emojis = gather_emojis(text)
    return len(list_of_emojis)
