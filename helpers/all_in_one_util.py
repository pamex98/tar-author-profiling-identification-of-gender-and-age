from nltk import sent_tokenize
from nltk.corpus import stopwords
import nltk
from nltk.tag import pos_tag

nltk.download("stopwords")
nltk.download("punkt")
STOP_WORDS = set(stopwords.words("english"))

import numpy as np
import re
from itertools import groupby
from nltk.tokenize import word_tokenize
import emoji

# https://www.journaldev.com/23788/python-string-module
import string as string_module


def count_sentences(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(sent_tokenize(text))


def count_chars(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(text)


def count_spaces(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    spaces = re.findall(r" ", text)
    return len(spaces)


def count_characters_excluding_spaces(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(text) - count_spaces(text)


### Number of characters without spaces
def gather_duplicates(text: str) -> dict:
    if not isinstance(text, str):
        return []

    tokenized_text = word_tokenize(text.lower())
    sorted_tokenized_text = sorted(tokenized_text)
    duplicates = {}
    for _, (value, group) in enumerate(groupby(sorted_tokenized_text)):
        frequency = len(list(group))
        if frequency > 1:
            duplicates.update({value: frequency})

    return duplicates


### Duplicates
def count_duplicates(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(gather_duplicates(text))


### Count words
def gather_words(text: str) -> list:
    if not isinstance(text, str):
        return []

    return re.findall(r"\b[^\d\W]+\b", text)


def count_words(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan
    return len(gather_words(text))


### Emojis
def gather_emojis(text: str) -> list:
    if (not isinstance(text, str)) or (len(text.strip()) == 0):
        return []

    emoji_expaned_text = emoji.demojize(text)
    return re.findall(r"\:(.*?)\:", emoji_expaned_text)


def count_emojis(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    list_of_emojis = gather_emojis(text)
    return len(list_of_emojis)


### Numbers
def gather_whole_numbers(text: str) -> list:
    if not isinstance(text, str):
        return []

    return re.findall(r"[0-9]+", text)


def count_whole_numbers(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(gather_whole_numbers(text))


### Alphanumeric
def gather_alpha_numeric(text: str) -> list:
    if not isinstance(text, str):
        return []

    return re.findall("[A-Za-z0-9]", text)


def count_alpha_numeric(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(gather_alpha_numeric(text))


### Non-alphanumeric
def gather_non_alpha_numeric(text: str) -> list:
    if not isinstance(text, str):
        return []

    return re.findall("[^A-Za-z0-9]", text)


def count_non_alpha_numeric(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(gather_non_alpha_numeric(text))


ADDITIONAL_SYMBOLS = "£€±§"


### Punctuations
def gather_punctuations(text: str) -> list:
    if not isinstance(text, str):
        return []

    line = re.findall("[" + string_module.punctuation + ADDITIONAL_SYMBOLS + "]*", text)
    string = "".join(line)
    return list(string)


def count_punctuations(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(gather_punctuations(text))


### Stop words
def gather_stop_words(text: str) -> list:
    if not isinstance(text, str):
        return []

    word_tokens = word_tokenize(text)
    return [word for _, word in enumerate(word_tokens) if word in STOP_WORDS]


def count_stop_words(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(gather_stop_words(text))


### Dates
def gather_dates(text: str, date_format: str = "dd/mm/yyyy") -> list:
    if not isinstance(text, str):
        return []

    ddmmyyyy = r"\b(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/([0-9]{4})\b"
    mmddyyyy = r"\b(1[0-2]|0[1-9])/(3[01]|[12][0-9]|0[1-9])/([0-9]{4})\b"
    regex_list = {"dd/mm/yyyy": ddmmyyyy, "mm/dd/yyyy": mmddyyyy}
    return re.findall(regex_list[date_format], text)


def count_dates(text: str) -> int:
    if not isinstance(text, str):
        return np.Nan

    return len(gather_dates(text))


nltk.download("averaged_perceptron_tagger")


def gather_nouns(sentence: str):
    if not isinstance(sentence, str) or len(sentence) == 0:
        return []
    emoji_decoded = (
        emoji.demojize(sentence, delimiters=("", "")).lower().strip()
    )  # Decoding Emoji's
    # import pdb; pdb.set_trace();
    token = word_tokenize(emoji_decoded)
    tags = list(
        filter(lambda x: re.match(r"(JJ|NN|NNP)", x[1]), pos_tag(token))
    )  # using RegEx to to check for Noun Phases.
    return tags


def count_noun_phase(text: str):
    if not isinstance(text, str):
        return np.Nan

    return len(gather_nouns(text))
